<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::get('/', 'CobaController@index')->name('coba');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/biodata', 'HomeController@formBiodata');
Route::post('/input_biodata', 'HomeController@inputBiodata')->name('input_biodata');
Route::post('/edit_biodata', 'HomeController@formEditBiodata')->name('edit_biodata');
Route::get('/deteksi_autis', 'HomeController@formDeteksiAutis')->name('deteksi_autis');
Route::get('/deteksi_adhd', 'HomeController@formDeteksiAdhd')->name('deteksi_adhd');
Route::post('/input_autis', 'HomeController@inputAutis')->name('input_autis');
Route::post('/input_adhd', 'HomeController@inputAdhd')->name('input_adhd');
Route::get('/intervensi_autis', 'HomeController@intervensiAutis')->name('intervensi_autis');
Route::get('/intervensi_adhd', 'HomeController@intervensiAdhd')->name('intervensi_adhd');
Route::get('/belajar_autis', 'HomeController@belajarAutis')->name('belajar_autis');
Route::get('/belajar_adhd', 'HomeController@belajarAdhd')->name('belajar_adhd');

Route::get('/list_dokter', 'HomeController@listDokter')->name('list_dokter');
Route::get('/data_dokter', 'AdminController@dataDokter')->name('data_dokter');
Route::get('/delete_dokter/{id}', 'AdminController@deleteDokter')->name('delete_dokter');
Route::post('/update_dokter', 'AdminController@updateDokter')->name('update_dokter');


Route::get('/list_pasien', 'HomeController@listPasien')->name('list_pasien');
Route::get('/data_pasien', 'AdminController@dataPasien')->name('data_pasien');
Route::post('/update_pasien', 'HomeController@updatePasien')->name('update_pasien');
Route::get('/delete_pasien/{id}', 'AdminController@deletePasien')->name('delete_pasien');



Route::get('/chats', 'HomeController@chats');
Route::get('/newchats/{id}', 'HomeController@newChats')->name('newchat');
Route::get('/chats/{id}', 'HomeController@chat');
Route::get('/messages/{id}', 'HomeController@fetchAllMessages');
Route::post('/messages', 'HomeController@sendMessage');



