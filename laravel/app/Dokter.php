<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dokter extends Model
{
    protected $table = 'dokter';
    
    protected $fillable = [
        'id_user', 
        'nama',
        'spesialis',
        'gambar',
    ];

    public function user(){
        return $this->hasOne('App\User');
    }
}
