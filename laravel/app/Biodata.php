<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biodata extends Model
{
    protected $table = 'biodata';
    
    protected $fillable = [
        'nama', 
        'nama_ibu', 
        'tanggal_lahir_ibu',
        'usia_ibu',
        'pekerjaan_ibu',
        'nama_anak', 
        'tanggal_lahir_anak',
        'usia_anak',
        'riwayat_kelahiran',
        'bantuan_melahirkan', 
        'riwayat_penyakit_ibu',
        'ibu_merokok',
        'ibu_akohol',
        'ibu_obat', 
        'kelainan_kehamilan',
        'usia_anak_membeo',
        'usia_anak_berjalan',
        'riwayat_sakit_anak', 
        'answer2',
        'user_id'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
