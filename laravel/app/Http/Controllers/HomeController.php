<?php

namespace App\Http\Controllers;

use App\Biodata;
use App\DetailAdhd;
use App\DetailAutis;
use App\DeteksiAdhd;
use App\DeteksiAutis;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Chat;
use App\ChatTransaction;
use App\Dokter;
use App\Events\ChatEvent;
use Ramsey\Uuid\Uuid;







class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    
    public function updatePasien(Request $request){

        $user = Biodata::where('user_id','=',$request->id)->first();

        $user->fill($request->all());

        $user->save();

        return redirect()->route('home');
    }

    public function listDokter(){
        $dokter = Dokter::get();
        foreach ($dokter as $d){
            $cek = ChatTransaction::where('id_user1','=',Auth::user()->id);
            if($cek->count() != 0){
                $d->chat = $cek->first()->id_chat;
            }else{
                $d->chat = "null";
            }
        }
        return view('page/list_dokter', ['dokter' => $dokter]);
    }

    public function listPasien(){
        $client = DB::table('chat_client')->where('id_user2','=',Auth::user()->id)->get(); 
        foreach($client as $d){
            $cek = Chat::where('user_id','=',$d->id_user1)->orderBy('id', 'desc');
            if($cek->count() != 0){
                $d->chat = $cek->first()->message;
            }else{
                $d->chat = "";
            }
        }
        return view('page/list_client', ['client' => $client]);
    }


    public function formEditBiodata(){
        
    }
    
    public function newChats($id){
        $uuid1 = Uuid::uuid1();
        ChatTransaction::create([
            'id_user1' => Auth::user()->id ,
            'id_user2' => $id ,
            'id_chat' => $uuid1->toString()
        ]);

        return redirect('/chats/'.$uuid1->toString());
    }

    public function formBiodata(){
        return view('page/biodata');
    }

    public function belajarAdhd(){
        return view('page.belajar_adhd');

    }

    public function belajarAutis(){
        return view('page.belajar_autis');
    }

    

    public function index()
    {
        $user = User::where('id','=',Auth::user()->id)->first();
        return view('home', ['user' => $user]);
    }

    public function intervensiAdhd(){
        $user = User::where('id','=',Auth::user()->id)->first();
        if(DetailAdhd::where([['id_user','=',Auth::user()->id],['id_jawaban','>','3']])->count() > 7 ){
            $user->tipe="rendah";           
        }
        return view('page.intervensi_adhd', ['user' => $user]);
    }

    public function intervensiAutis(){
        $user = User::where('id','=',Auth::user()->id)->first();
        if(DB::table('autis_besar')->where('id_user','=',Auth::user()->id)->count() > 5 ){
            $user->tipe="sedang";           
        }elseif(DetailAutis::where([['id_user','=',Auth::user()->id],['id_jawaban','>','3']])->count() > 7 ){
            $user->tipe="rendah";           
        }else{
            $user->tipe="tidak";  
            if(DB::table('autis_mata')->where('id_user','=',Auth::user()->id)->count() > 0 ){
                $user->mata = 'v';
            }else{
                $user->mata = 'x';
            }
            if(DB::table('autis_imitasi')->where('id_user','=',Auth::user()->id)->count() > 0 ){
                $user->imitasi = 'v';
            }else{
                $user->imitasi = 'x';
            }
            if(DB::table('autis_reseptif')->where('id_user','=',Auth::user()->id)->count() > 0 ){
                $user->reseptif = 'v';
            }else{
                $user->reseptif = 'x';
            }
            if(DB::table('autis_ekspresif')->where('id_user','=',Auth::user()->id)->count() > 0 ){
                $user->ekspresif = 'v';
            }else{
                $user->ekspresif = 'x';
            }
            $biodata = Biodata::where('id_user','=',Auth::user()->id)->first();
            if($biodata->usia_anak > 12 ){
                $user->akademik = 'v';
            }else{
                $user->akademik = 'x';
            }
        }

        return view('page.intervensi_autis', ['user' => $user]);
    }

    public function inputAutis(Request $request){
        $pertanyaan = DeteksiAutis::get();
        $i = 0;
        DetailAutis::where('id_user',Auth::user()->id)->delete();
        foreach($pertanyaan as $p){
            DetailAutis::create([
                'id_user' => Auth::user()->id ,
                'id_pertanyaan' => $p->id ,
                'id_jawaban' => $request->radio[$i++]
            ]);
        }

        return redirect()->route('intervensi_autis');

    }

    public function inputAdhd(Request $request){
        $pertanyaan = DeteksiAdhd::get();
        $i = 0;
        DetailAdhd::where('id_user',Auth::user()->id)->delete();
        foreach($pertanyaan as $p){
            DetailAdhd::create([
                'id_user' => Auth::user()->id ,
                'id_pertanyaan' => $p->id ,
                'id_jawaban' => $request->radio[$i++]
            ]);
        }

        return redirect()->route('intervensi_adhd');
        
    }

    public function formDeteksiAutis()
    {
        $pertanyaan = DeteksiAutis::get();
        return view('page.deteksi_autis', ['pertanyaan' => $pertanyaan]);
    }

    public function formDeteksiAdhd()
    {
        $pertanyaan = DeteksiAdhd::get();
        return view('page.deteksi_adhd', ['pertanyaan' => $pertanyaan]);
    }

    

    public function inputBiodata(Request $request){

        Biodata::create([
        'nama' => $request->nama   ,
        'nama_ibu' => $request->nama_ibu   ,
        'tanggal_lahir_ibu' => $request->tanggal_lahir_ibu   , 
        'usia_ibu' => $request->usia_ibu   ,
        'pekerjaan_ibu' => $request->pekerjaan_ibu   ,
        'nama_anak' => $request->nama_anak   , 
        'tanggal_lahir_anak' => $request->tanggal_lahir_anak   ,
        'usia_anak' => $request->usia_anak ,
        'riwayat_kelahiran' => $request->riwayat_kelahiran ,
        'bantuan_melahirkan' => $request->bantuan_melahirkan , 
        'riwayat_penyakit_ibu' => $request->riwayat_penyakit_ibu ,
        'ibu_merokok' => $request->ibu_merokok ,
        'ibu_akohol' => $request->ibu_akohol ,
        'ibu_obat' => $request->ibu_obat , 
        'kelainan_kehamilan' => $request->kelainan_kehamilan ,
        'usia_anak_membeo' => $request->usia_anak_membeo ,
        'usia_anak_berjalan' => $request->usia_anak_berjalan ,
        'riwayat_sakit_anak' => $request->riwayat_sakit_anak , 
        'user_id' =>  Auth::user()->id            
        ]);

        return redirect()->route('home');
    }

    public function chats()
    {
    	return view('chat.chat');
    }

    public function chat($id)
    {
    	return view('chat.chat', ['id' => $id]);
    }


    public function fetchAllMessages($id)
    {
        $chats = Chat::with('user')->where('chat_id','=',$id)->get();
        foreach($chats as $chat){
        if($chat->user->role == 'client'){
                $biodata = Biodata::where('user_id','=',$chat->user->id)->first();
                $chat->user->name = $biodata->nama;
          
        }else{
                $dokter = Dokter::where('user_id','=',$chat->user->id)->first();
                $chat->user->name = $dokter->nama; 
        }
        }
        

        return $chats;


        // return Chat::with('user')->get();

    }

    public function sendMessage(Request $request)
    {
    	$chat = auth()->user()->messages()->create([
            'message' => $request->message,
            'chat_id' => $request->chatid
        ]);

    	broadcast(new ChatEvent($chat->load('user')))->toOthers();

    	return ['status' => 'success'];
    }
}
