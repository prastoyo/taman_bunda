<?php

namespace App\Http\Controllers;

use App\Biodata;
use App\DetailAdhd;
use App\DetailAutis;
use App\DeteksiAdhd;
use App\DeteksiAutis;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Chat;
use App\ChatTransaction;
use App\Dokter;
use App\Events\ChatEvent;
use Ramsey\Uuid\Uuid;

class AdminController extends Controller
{
    public function dataPasien(){
        $users = User::where('role','=','client')->get();

        return view('page/data_pasien', ['users' => $users]);



        // foreach($users as $user){
        //     $user->dokter?$user->dokter->nama:'kosong';
        //     echo "</br>";
        // }
    }

    public function dataDokter(){
        $users = User::where('role','=','dokter')->get();

        return view('page/data_dokter', ['users' => $users]);
    }

    public function deleteDokter($id){
        

        $user = User::find($id);
        $user->delete();

        $dokter = Dokter::where('user_id','=',$id);
        $dokter->delete();

        return redirect()->route('data_dokter');

    }

    public function deletePasien($id){
        

        $user = User::find($id);
        $user->delete();

        $biodata = Biodata::where('user_id','=',$id);
        $biodata->delete();

        return redirect()->route('data_user');

    }

    public function updateDokter(Request $request){

        $dokter = Dokter::where('user_id','=',$request->id)->first();

        $dokter->fill($request->all());

        $dokter->save();

        return redirect()->route('data_dokter');
    }
}
