<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailAutis extends Model
{
    protected $table = 'detail_autis';
    
    protected $fillable = [
        'id', 
        'id_user',
        'id_pertanyaan',
        'id_jawaban'
    ];
}
