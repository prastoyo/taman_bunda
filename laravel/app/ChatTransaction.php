<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatTransaction extends Model
{
    protected $table = 'chat_transaction';
    
    protected $fillable = [
        'id', 
        'id_chat',
        'id_user1',
        'id_user2'
    ];

  
}

