<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeteksiAdhd extends Model
{
    protected $table = 'pertanyaan_adhd';
    
    protected $fillable = [
        'id', 
        'pertanyaan',
    ];
}
