<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailAdhd extends Model
{
    protected $table = 'detail_adhd';
    
    protected $fillable = [
        'id', 
        'id_user',
        'id_pertanyaan',
        'id_jawaban'
    ];
}
