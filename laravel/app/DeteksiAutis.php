<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeteksiAutis extends Model
{
    protected $table = 'pertanyaan_autis';
    
    protected $fillable = [
        'id', 
        'pertanyaan',
    ];
}
