<ol style="list-style-type: upper-alpha;" start="2">
<li><span style="font-family: arial, helvetica, sans-serif;"><strong>Kemampuan bahasa reseptif&nbsp;</strong></span></li>
</ol>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">1. Tahap 1 : mengikuti perintah sederhana&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Duduk!&rdquo;&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Berdiri!&rdquo;&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Ke sini!&rdquo;&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Tangan kebawah!&rdquo;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;lambaikan tangan!&rdquo;&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;peluk saya&rdquo;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;tangan keatas!&rdquo;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;berputar!&rdquo;&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;lompat!&rdquo;&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;cium saya!&rdquo;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;lempar (bola)!&rdquo;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;tutup pintu!&rdquo;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;matikan lampu!&rdquo;&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;ambilkan tisu!&rdquo;&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;matikan radio!&rdquo;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;letakkan papan!&rdquo;&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;toss!&rdquo;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;hentakkan kaki!&rdquo;&nbsp;</span><br /><span style="font-family: arial, helvetica, sans-serif;">Catatan :Tanda petik pada aktivitas berarti instruksi harus diberikan dengan kata-kata yang berada dalam tanda petik. Instruksi diberikan bersamaan dengan model atau contoh dari orangtua. Prompt diberikan sesudah instruksi ke-2 dan pada bagian-bagian objek lain dapat diperkaya sendiri oleh orangtua</span></li>
</ol>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">2. Tahap II : Identifikasi bagian-bagian tubuh&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;pegang kepala!&rdquo;&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;pegang kaki!&rdquo;&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;pegang perut!&rdquo;&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;pegang hidung!&rdquo;&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;pegang bibir!&rdquo;&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;pegang paha!&rdquo;&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;pegang mata!&rdquo;&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;pegang kuping!&rdquo;&nbsp;</span><br /><span style="font-family: arial, helvetica, sans-serif;">Catatan :Tanda petik pada aktivitas berarti instruksi harus diberikan dengan kata-kata yang berada dalam tanda petik. Instruksi diberikan bersamaan dengan model atau contoh dari orangtua. Prompt diberikan sesudah instruksi ke-2 dan pada bagian-bagian objek lain dapat diperkaya sendiri oleh orangtua</span></li>
</ol>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">3. Tahap III : Identifikasi objek&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
<li><span style="font-family: arial, helvetica, sans-serif;">Siapkan 10 macam benda (misalnya sendok, garpu, gelas, pensil, spidol, bola, blok, boneka dan lain-lain, benda-benda dapat ditambah dan diubah)</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Anak memegang benda sesuai intruksi&nbsp;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : instruksi &ldquo;pegang &hellip; (nama objek)!&rdquo; dapat dilanjutkan dengan mencocokan untuk itu siapkan pasangannya.&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">4. Tahap IV : Identfiikasi gambar&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
<li><span style="font-family: arial, helvetica, sans-serif;">Siapkan 10 gambar-gambar yang sesuai dengan benda-benda seperti pada tahapan III&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Anak mampu memegang gambar yang sesuai instruksi&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Anak mampu mencocokan benda dan gambar&nbsp;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : instruksi yang dipakai adalah &ldquo;cocokkan!&rdquo;&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">5. Tahap V : Identifikasi anggota keluarga atau orang dekat&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
<li><span style="font-family: arial, helvetica, sans-serif;">Siapkan foto dan orangnya. Instruksi untuk foto sama dengan diatas&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Anak mampu memegang yang sesuai instruksi&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Anak mendekati orang yang sesuai dengan instruksi&nbsp;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : instruksi yang digunakan &ldquo;pegang &hellip; (nama objek)!&rdquo; dan instruksi kan juga dengan menggunakan nama anak&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">6. Tahap VI : Mengikuti insruksi kata kerja dan identifikasi pada gambar yang sesuai&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
<li><span style="font-family: arial, helvetica, sans-serif;">Berdiri&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Duduk&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Bertepuk tangan&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Bergoyang&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memakan&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Meminum&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Berbalik/.berputar&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Melompat&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memeluk&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mencium&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Meniup&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Tidur&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengetuk&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Membaca&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menggambar&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menangis&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menyikat&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Melempar&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Berjalan&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menyepak&nbsp;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : instruksi yang dapat digunakan adalah &ldquo;ayo &hellip;!&rdquo; dan respon yang diharapkan dari anak adalah melakukan yang di instruksikan&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">7. Tahap VII : identifikasi objek-objek di lingkungan&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
<li><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk meja&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk kursi&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk jendela&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk lantai&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk tembok&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk pintu&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk karpet</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk lampu&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk tangga&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk papan tulis&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk gorden&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk kulkas&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk panci&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk bak air atau kolam&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk kamar mandi&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk tempat tidur atau kasur&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk bantal&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk guling&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk lemari&nbsp;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : instruksi dapat menggunakan &ldquo;pegang &hellip;!&rdquo; atau &ldquo;tunjuk&hellip;&rdquo; dan diharapkan respon anak dapat menunjuk gambar yang sesuai atau berjalan dan memegang objek yang sesuai.&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">8. Tahap VIII : identifikasi gambar dalam buku&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
<li><span style="font-family: arial, helvetica, sans-serif;">Siapkan gambar-gambar dalam buku (minimal 10 gambar)&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Anak mampu menunjuk gambar dalam buku sesuai dengan instruksi&nbsp;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : instruksi yang digunakan &ldquo;tunjuk &hellip;!&rdquo; dan diharapkan respon anak dapat menunjuk dengan benar.&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">9. Tahap IX : Identifikasi objek menurut fungsinya&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
<li><span style="font-family: arial, helvetica, sans-serif;">Menulis menggunakan &hellip; (pensil)&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Minum dari &hellip; (cangkir)&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Makan menggunakan &hellip; (sendok)&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menggunting menggunakan &hellip; (gunting)&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Membaca &hellip; (buku)&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Tidur di atas &hellip; (tempat tidur)&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Duduk di &hellip; (kursi)&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menelepon menggunakan (telepon)&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mewarnai dengan &hellip; (krayon)&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mandi dengan &hellip; (sabun)&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menyapu menggunakan &hellip; (sapu)&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengelap mulut dengan &hellip; (tisu)&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menendang &hellip; (bola)&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menyisir rambut dengan &hellip; (sisir)&nbsp;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Instruksi dapat dikembangan sesuai dengan keadaan orangtua&nbsp;</span></p>
<ol style="list-style-type: undefined; margin-left: 0.25in;">
<li><span style="font-family: arial, helvetica, sans-serif;">Tahap X : Identifikasi kepemilikan&nbsp;</span></li>
</ol>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
<li><span style="font-family: arial, helvetica, sans-serif;">Siapkan seorang dengan pakaian yang cukup lengkap dan segera lakukan generalisasi dengan orang lain&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menyentuh atau memgang bagian-bagian tubuh atau pakaian seseorang</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Contoh dapat disesuaikan dengan kondisi &nbsp;</span></p>
<ol style="list-style-type: undefined; margin-left: 0.25in;">
<li><span style="font-family: arial, helvetica, sans-serif;">Tahap XI : Identifikasi suara-suara di lingkungan&nbsp;</span></li>
</ol>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi bunyi dering telepon</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi bunyi detak lonceng</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi suara kodok</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi bunyi bersin</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi suara gonggongan anjing</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi suara bebek</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi suara tangis bayi</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi suara kucing</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi suara piano</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi suara klakson mobil</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi suara burung</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi bunyi bola memantul</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi suara mobil berangkat</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi bunyi suara air</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi suara babi</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi suara sapi</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi bunyi-bunyi bel rumah</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentfikasi bunyi suara terompet</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi orang menghirup minuman</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi bunyi ketukan palu</span></li>
</ol>
<ol style="list-style-type: upper-alpha;" start="4">
<li><span style="font-family: arial, helvetica, sans-serif;"><strong>Kemampuan Pra-Akademik&nbsp;</strong></span></li>
</ol>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">1. Tahap I : Mencocokkan (<em>matching</em>)&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
<li><span style="font-family: arial, helvetica, sans-serif;">Mencocokkan objek-objek yang identik&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mencocokkan gambar-gambar yang identik&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mencocokkan objek dengan gambar atau sebaliknya&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mencocokkan berdasarkan warna, bentuk dan angka&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi benda-benda yang tidak identic (dalam satu kelompok)&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi hubungan antara objek satu dengan objek lainnya yang berbeda (misalnya: pensil dan kertas)</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : siapakan alat peraga yang berpasangan dan instruksi yang digunakan adalah &ldquo;cocokkan!&rdquo; atau bahasa yang biasa dipakai. Respon yang diharapkan anak dapat mencocokan dan memilih pasangan dari suatu objek&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">2. Tahap II Menyelesaikan aktivitas sederhana secara mandiri&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
<li><span style="font-family: arial, helvetica, sans-serif;">Melompat&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Berputar&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Merentangkan tangan&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Berbaris&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Duduk dilantai&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menepuk lantai&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengetuk pintu&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Berjalan mengelilingi kursi&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Tidur di lantai&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Meletakkan tangan di atas paha&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memutar tangan pada pergelangan&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Merangkak&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menyentuh jari kaki&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Berlari dan berhenti&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengangkat satu kaki&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Melompat dengan dua kaki&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menirukan gerakan pesawat terbang&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Merangkak di bawah meja&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengangkat kursi&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menyepak bola&nbsp;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Instruksi yang dipakai &ldquo;lakukan ini!&rdquo; dan respon yang diharapkan adalah anak dapat melakukan dengan benar.&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">3. Tahap III Identifikasi warna&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi warna merah&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi warna kuning&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi warna biru&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi warna hitam&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi warna putih&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi warna hijau&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi warna ungu&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi warna jingga&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi warna merah muda&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi warna cokelat&nbsp;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Mulai dengan tiga warna dasar. Objek berbentuk kotak, sama besar dan dari material yang sama. Lakukan prosedur empat langkah untuk setiap warna</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">4. Tahap IV &nbsp;Identifikasi bentuk</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi bentu bola (lingkaran)</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi bentuk kotak (bujursangkar)</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi bentuk segitiga&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi bentuk bintang&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi bentuk hati&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi bentuk (oval)</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi bentuk berlian&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi bentuk busur&nbsp;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Mulailah dengan bentuk bintang. Lakukan prosedur empat langkah untuk setiap bentuk.&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">V. Tahap V : Identifikasi huruf&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
<li><span style="font-family: arial, helvetica, sans-serif;">Huruf capital</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Huruf kecil&nbsp;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Lakukan prosedur empat langkah untuk setiap huruf</span></p>
<p style="margin-left: 20px;"><span style="font-family: arial, helvetica, sans-serif;">VI. Tahap VI : Identifikasi angka&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
<li><span style="font-family: arial, helvetica, sans-serif;">Angka 1-10&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Angka 11-20&nbsp;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Lakukan prosedur empat langkah untuk setiap huruf</span></p>