<ol type="A">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;"><strong>Terapi Wicara&nbsp;</strong></span></p>
</li>
</ol>
<ol>
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;"><strong>Kemampuan bahasa reseptif (kognitif)&nbsp;</strong></span></p>
</li>
</ol>
<ol type="a">
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap 1 : Mengikuti Perintah sederhana</span></p>
</li>
</ol>
<ol style="margin-left: 20px;">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Duduk!&rdquo;</span></p>
</li>
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Berdiri!&rdquo;</span></p>
</li>
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Ke sini!&rdquo;</span></p>
</li>
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Tangan kebawah!&rdquo;</span></p>
</li>
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;lambaikan tangan!&rdquo;</span></p>
</li>
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;peluk saya&rdquo;</span></p>
</li>
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;tangan keatas!&rdquo;</span></p>
</li>
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;berputar!&rdquo;</span></p>
</li>
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;lompat!&rdquo;</span></p>
</li>
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;cium saya!&rdquo;</span></p>
</li>
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;lempar (bola)!&rdquo;</span></p>
</li>
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;tutup pintu!&rdquo;</span></p>
</li>
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;matikan lampu!&rdquo;</span></p>
</li>
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;ambilkan tisu!&rdquo;</span></p>
</li>
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;matikan radio!&rdquo;</span></p>
</li>
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;letakkan papan!&rdquo;</span></p>
</li>
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;toss!&rdquo;</span></p>
</li>
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;hentakkan kaki!&rdquo;</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan :</span></p>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">Tanda petik pada aktivitas berarti instruksi harus diberikan dengan kata-kata yang berada dalam tanda petik. Instruksi diberikan bersamaan dengan model atau contoh dari orangtua. Prompt diberikan sesudah instruksi ke-2 dan pada bagian-bagian objek lain dapat diperkaya sendiri oleh orangtua</span></p>
<ol start="2" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap II : Identifikasi bagian-bagian tubuh</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;pegang kepala!&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;pegang kaki!&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;pegang perut!&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;pegang hidung!&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;pegang bibir!&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;pegang paha!&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;pegang mata!&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;pegang kuping!&rdquo;</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan :</span></p>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">Tanda petik pada aktivitas berarti instruksi harus diberikan dengan kata-kata yang berada dalam tanda petik. Instruksi diberikan bersamaan dengan model atau contoh dari orangtua. Prompt diberikan sesudah instruksi ke-2 dan pada bagian-bagian objek lain dapat diperkaya sendiri oleh orangtua</span></p>
<ol start="3" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap III : Identifikasi objek</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Siapkan 10 macam benda (misalnya sendok, garpu, gelas, pensil, spidol, bola, blok, boneka dan lain-lain, benda-benda dapat ditambah dan diubah)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Anak memegang benda sesuai intruksi</span><br /><span style="font-family: arial, helvetica, sans-serif;">Catatan : instruksi &ldquo;pegang &hellip; (nama objek)!&rdquo; dapat dilanjutkan dengan mencocokan untuk itu siapkan pasangannya.</span></p>
</li>
</ol>
<ol start="4" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap IV : Identfiikasi gambar</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Siapkan 10 gambar-gambar yang sesuai dengan benda-benda seperti pada tahapan III</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Anak mampu memegang gambar yang sesuai instruksi</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Anak mampu mencocokan benda dan gambar</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : instruksi yang dipakai adalah &ldquo;cocokkan!&rdquo;</span></p>
<ol start="5" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap V : Identifikasi anggota keluarga atau orang dekat</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Siapkan foto dan orangnya. Instruksi untuk foto sama dengan diatas</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Anak mampu memegang yang sesuai instruksi</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Anak mendekati orang yang sesuai dengan instruksi</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : instruksi yang digunakan &ldquo;pegang &hellip; (nama objek)!&rdquo; dan instruksi kan juga dengan menggunakan nama anak</span></p>
<ol start="6" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap VI : Mengikuti insruksi kata kerja dan identifikasi pada gambar yang sesuai</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Berdiri</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Duduk</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Bertepuk tangan</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Bergoyang</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memakan</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Meminum</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Berbalik/.berputar</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Melompat</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memeluk</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mencium</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Meniup</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tidur</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengetuk</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Membaca</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menggambar</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menangis</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menyikat</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Melempar</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Berjalan</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menyepak</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 60px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : instruksi yang dapat digunakan adalah &ldquo;ayo &hellip;!&rdquo; dan respon yang diharapkan dari anak adalah melakukan yang di instruksikan</span></p>
<ol start="7" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap VII : identifikasi objek-objek di lingkungan</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk meja</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk kursi</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk jendela</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk lantai</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk tembok</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk pintu</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk karpet</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk lampu</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk tangga</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk papan tulis</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk gorden</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk kulkas</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk panci</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk bak air atau kolam</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk kamar mandi</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk tempat tidur atau kasur</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk bantal</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk guling</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memegang/menunjuk lemari</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : instruksi dapat menggunakan &ldquo;pegang &hellip;!&rdquo; atau &ldquo;tunjuk&hellip;&rdquo; dan diharapkan respon anak dapat menunjuk gambar yang sesuai atau berjalan dan memegang objek yang sesuai.</span></p>
<ol start="8" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap VIII : identifikasi gambar dalam buku</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Siapkan gambar-gambar dalam buku (minimal 10 gambar)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Anak mampu menunjuk gambar dalam buku sesuai dengan instruksi</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 60px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : instruksi yang digunakan &ldquo;tunjuk &hellip;!&rdquo; dan diharapkan respon anak dapat menunjuk dengan benar.</span></p>
<ol start="9" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap IX : Identifikasi objek menurut fungsinya</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menulis menggunakan &hellip; (pensil)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Minum dari &hellip; (cangkir)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Makan menggunakan &hellip; (sendok)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menggunting menggunakan &hellip; (gunting)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Membaca &hellip; (buku)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tidur di atas &hellip; (tempat tidur)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Duduk di &hellip; (kursi)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menelepon menggunakan (telepon)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mewarnai dengan &hellip; (krayon)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mandi dengan &hellip; (sabun)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menyapu menggunakan &hellip; (sapu)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengelap mulut dengan &hellip; (tisu)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menendang &hellip; (bola)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menyisir rambut dengan &hellip; (sisir)</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Instruksi dapat dikembangan sesuai dengan keadaan orangtua</span></p>
<ol start="10" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap X : Identifikasi kepemilikan</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Siapkan seorang dengan pakaian yang cukup lengkap dan segera lakukan generalisasi dengan orang lain</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menyentuh atau memgang bagian-bagian tubuh atau pakaian seseorang</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Contoh dapat disesuaikan dengan kondisi</span></p>
<ol start="11" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap XI : Identifikasi suara-suara di lingkungan</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi bunyi dering telepon</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi bunyi detak lonceng</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi suara kodok</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi bunyi bersin</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi suara gonggongan anjing</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi suara bebek</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi suara tangis bayi</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi suara kucing</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi suara piano</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi suara klakson mobil</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi suara burung</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi bunyi bola memantul</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi suara mobil berangkat</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi bunyi suara air</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi suara babi</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi suara sapi</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi bunyi-bunyi bel rumah</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengidentfikasi bunyi suara terompet</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi orang menghirup minuman</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengidentifikasi bunyi ketukan palu</span></p>
</li>
</ol>
<ol start="2">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;"><strong>Kemampuan bahasa ekspresif&nbsp;</strong></span></p>
</li>
</ol>
<ol type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap I : Menunjuk sesuatu yang diinginkan</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menunjuk objek kesulitan anak</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menunjuk objek kesukaan dan bukan kesukaan anak</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Meletakkan kedua objek di atas meja</span><br /><span style="font-family: arial, helvetica, sans-serif;">&nbsp;Catatan : Intruksi dapat dilakukan dengan menyebut nama anak &ldquo;(nama anak), mau apa?&rdquo; dan diharapkan respon anak adalah anak menunjuk objek yang disukai</span></p>
</li>
</ol>
<ol start="2" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap II : Menunjuk sesuatu yang diinginkan secara spontan</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Anak menunjuk kearah objek yang diinginkan tanpa instruksi verbal</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : coba dengan menggunakan tiga objek terlebih dahulu</span></p>
<ol start="3" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap III : Imitasi suara dan kata</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Papa&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Mama&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Mami&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Babi&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Kuda&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Sapi&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Mobil&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Motor&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Aku&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Kamu&rdquo;</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Respon yang diharapkan pada anak adalah anak dapat menirukan dengan benar</span></p>
<ol start="4" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap IV : Melabel Objek</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Melabel 10 macam objek</span></p>
</li>
</ol>
<ol start="5" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap V : Menyebutkan nama gambar</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menyebutkan nama-nama 10 macam gambar</span></p>
</li>
</ol>
<ol start="6" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap VI : Mengatakan objek yang diinginkan</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menunjuk dan menyebutkan objek yang diinginkan, minimal tiga (siapakan tiga macam benda kesukaan anak)</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 60px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Instruksi yang dapat digunakan &ldquo;kamu ingin apa?&rdquo; dan diharapkan respon anak adalah menjawab &ldquo;(bu), saya ingin &hellip;&rdquo;</span></p>
<ol start="7" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap VII : Menyatakan ingin atau tidak atas suatu objek dengan kata-kata &ldquo;ya&rdquo; dan &ldquo;tidak&rdquo;</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menyatakan &ldquo;ya&rdquo; atas objek yang disukai</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menyatakan &ldquo;tidak&rdquo; atas objek yang tidak disukai</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memilih objek yang disukai (objek yang disukai dan yang tidak disukai dicampur)</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 60px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Instruksi dapat menggunakan &ldquo;kamu mau&hellip;?&rdquo; dan diahrapkan respon dari anak adalah &ldquo;ya&rdquo; atau &ldquo;tidak&rdquo;. Pertanyaan dapat dikembangkan sendiri oleh orangtua disesuaikan dengan kondisi anak.</span></p>
<ol start="8" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap VIII : Melabel anggota keluarga atau orang dekat</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Melabel anggota keluarga atau orang dekat secara langsung atau menggunakan fotonya (minimal 3 orang)</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : instruksi dapat menggunakan &ldquo;siapa ini?&rdquo; dan diharapkan respon anak adalah dapat menyebutkan nama yang ditanyakan</span></p>
<ol start="9" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap IX : Membuat pilihan</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memilih objek yang (sangat) disukai dan yang (sangat) tidak disukai (minimal tiga objek)</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : instruksi dapat menggunakan &ldquo; mau ini atau ini?&rdquo; disesuaikan dengan bahasa yang digunakan sehari-hari dan diharapkan respon anak adalah dapat menunjuk serta membuat label.</span></p>
<ol start="10" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap X : Saling menyapa</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mulai dengan &ldquo;halo &hellip; (nama)&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memberi salam &ldquo;selamat pagi &hellip; (nama)&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memberi salam &ldquo;selamat siang &hellip; (nama)&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memberi salam &ldquo;selamat sore &hellip; (nama)&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memberi salam &ldquo;selamat malam &hellip; (nama)&rdquo;</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : instruksi dapat digunakan dengan &ldquo;tirukan&hellip;!&rdquo; dan diharapkan respon anak adalah menirukan dengan benar</span></p>
<ol start="11" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap XI : Menjawab pertanyaan sosial</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;siapa namamu?&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;berapa umurmu?&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;apa kabar &hellip; (nama anak)?&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;dimanakah rumahmu?&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;siapakah kakak/adikmu?&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;kamu mau main dengan siapa?&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;siapakah nama papa/mamamu?&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;kamu mau makan apa ?&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;di manakah sekolahmu ?&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;siapakh temanmu?&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;berapakah nomor teleponmu?&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;kamu mau minum apa?&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;kapankah hari ulang tahunmu?&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;siapakah nama gurumu?&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;apakah acara tv yang kamu sukai?&rdquo;</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">&ldquo;kamu senang bermain apa?&rdquo;</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Anak dapat dibantu untuk menjawab dengan benar . Anak dapat dibantu untuk mengingatnya. Lanjutkan hungga anak mampu menjawab secara mandiri.</span></p>
<ol start="12" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap XII : Menyebutkan kata kerja alam, gambar, orang lain dan diri sendiri</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Berdiri</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Duduk</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Bertepuk tangan</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Bergoyang</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Makan</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Minum</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Bertepuk atau berbalik</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Melompat</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memeluk</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mencium</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Meniup</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tidur</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengetuk</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Membaca</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menggambar</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Instruksi yang digunakan dapat berupa pertanyaan seperti &ldquo;apa yang ia kerjakan?&rdquo; , &ldquo;saya sedang apa?&rsquo; atau &ldquo;kamu sedang apa?&rdquo; dan diharapkan respon anak sesuai dengan label yang diberikan kepada pekerjaan atau kegiatan yang ditunjuk.</span></p>
<ol start="13" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap 13 : Melabel benda-benda melalui fungsinya</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menulis dengan &hellip; (pensil)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Minum dari &hellip; (cangkir/gelas)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Makan dengan &hellip; (sendok)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menggunting dengan &hellip; (gunting)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Memabaca &hellip; (buku)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tidur di atas &hellip; (tempat tidur)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Duduk diatas &hellip; (kursi)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Berbicara melalui &hellip; (telepon)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mewarnai dengan &hellip; (krayon)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mencuci dengan &hellip; (sabun)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menyapu dengan &hellip; (sapu)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menyeka mulut dengan &hellip; (kertas tisu)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Melempar &hellip; (bola)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menyisir rambut dengan &hellip; (sisir)</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Objek dapat diganti dengan yang sudah dikenal anak, instruksi yang digunakan &ldquo;kamu menulis dengan apa?&rdquo; atau &ldquo;apa yang kamu lempar?&rdquo; atau &ldquo;saya duduk di atas apa?&rdquo; dan diharapkan anak dapat melabel atau menyebutkan kegiatannya dengan benar.</span></p>
<ol start="14" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap 14 : Melabel kepemilikan</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Siapkan seseorang dengan pakaian yang cukup lengkap dan segera lakukan generalisasi dengan orang lain</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menyentuh atau memegang bagian-bagian tubuh atau pakaian seseorang</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menggunakan sufiks; -ku, -mu, -nya</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Instruksi dapat menggunakan &ldquo;baju siapa ini?&rdquo; dan dapat dikembangkan sesuai dengan kondisi. Respon yang diharapkan dari anak dapat menyebutkan objek dan pemiliknya</span></p>
<ol start="15" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap 15 : Melabel rasa manis, asin, asam, pahit dan tawar</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Merasakan rasa manis</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Merasakan rasa asin</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Merasakan rasa asam</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Merasakan rasa pahit</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Merasakan rasa tawar</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 60px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : setiap poin rasa dapat disesuaikan dengan kondisi atau dapat dicontohkan air gula untuk rasa manis, air garam untuk rasa asin, air cuka untuk rasa asam, air kopi untuk rasa pahit dan air putih untuk rasa tawar. Instruksi yang digunakan dapat dengan &ldquo;apa rasanya?&rdquo; dengan hasil respon anak yaitu anak mampu mengungkapkan rasa dari objek yang dikecap</span></p>
<ol start="16" type="a">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap 16 : Melabel sensasi halus dan kasar</span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Merasakan butiran halus tepung</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Merasakan butiran kasar pasir</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Merasakan sentuhan halus kapas</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Merasakan sentuhan kasar sikat</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Merasakan permukaan halus kaca</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Merasakan permukaan kasar kertas ampelas</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Instruksi yang digunakan bisa dengan &ldquo;bagaimana rasanya?&rdquo; dan respon anak diharapkan mampu menjawab dengan benar.</span></p>
<ol start="2" type="A">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;"><strong>Terapi bermain&nbsp;</strong></span></p>
</li>
</ol>
<ol>
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap 1 : Melatih fokus dan anak yang sulit diam</span></p>
</li>
</ol>
<ol type="a">
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Permainan freeze time</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 56px;"><span style="font-family: arial, helvetica, sans-serif;">Instruksi :</span></p>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 56px;"><span style="font-family: arial, helvetica, sans-serif;">Meminta anak untuk berpose sesuka hati anak secara tiba-tiba dalam waktu tertentu. Untuk awal 10 detik yang kemudian ditingkatkan beberapa detik.</span></p>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 56px;"><span style="font-family: arial, helvetica, sans-serif;">Respon anak :</span></p>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 56px;"><span style="font-family: arial, helvetica, sans-serif;">Mengikuti instruksi untuk diam sejenak dan melatih anak untuk menahan diri melakukan gerakan-gerkaan berlebih serta melatih fokus anak</span></p>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 56px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan :</span></p>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 56px;"><span style="font-family: arial, helvetica, sans-serif;">Permainan dalam melatih fokus anak tidak hanya dengan permianan puzzle, orang tua dapat mengembangkan sesuai dengan arahan ahli.</span></p>
<ol start="2">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap 2 : Melatih duduk dan konstrasi anak</span></p>
</li>
</ol>
<ol type="a">
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Bermain puzzle</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mewarnai</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Melukis dengan jari</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Bermain air</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan :</span></p>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">Agar anak mau duduk maka kegiatan harus dilakukan di meja dan dilakukan dengan disiplin.</span></p>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">Jenis permainan dapat disesuiakan dengan kreasi anda sebagai orangtua anak. Dalam daftar permainan disini hanya sebagai acuan.</span></p>
<ol start="3">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap 3 : Melatih anak untuk tetap fokus mengerjakan sebuah tugas</span></p>
</li>
</ol>
<ol type="a">
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Bermain dengan menyanyi</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 60px;"><span style="font-family: arial, helvetica, sans-serif;">Instruksi :</span></p>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 60px;"><span style="font-family: arial, helvetica, sans-serif;">Tugas dapat diatur menjadi sebuah lagu. Untuk memberikan daya ingat yang kuat pada anak, anda dapat memilih salah satu melodi favoritnya disesuaikan dengan kegiatan yang akan diajarkan.</span></p>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 60px;"><span style="font-family: arial, helvetica, sans-serif;">Contoh :</span></p>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 60px;"><span style="font-family: arial, helvetica, sans-serif;">Misalnya anda dapat menyanyikan &ldquo;clean up song&rdquo; untuk membantunya mengingat ia sedang membersihkan mainannya.</span></p>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 60px;"><span style="font-family: arial, helvetica, sans-serif;">Respon anak :</span></p>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 60px;"><span style="font-family: arial, helvetica, sans-serif;">Anak mampu terangsang dan dapat mengingat ekgiatan yang harus dilakukan saat menyanyikan lagu tersebut</span></p>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 60px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan :</span></p>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 60px;"><span style="font-family: arial, helvetica, sans-serif;">Permainan dalam melatih anak untuk tetap fokus mengerjakan sebuah tugas tidak hanya dengan bermain dengan menyanyi, orang tua dapat mengembangkan sesuai dengan arahan ahli.</span></p>
<ol start="4">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap 4 : Melatih an aktivitas fisik anak untuk mngurangi hiperaktivitas anak</span></p>
</li>
</ol>
<ol>
<li style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 20px;"><span style="font-family: arial, helvetica, sans-serif;">Bersepeda</span></li>
<li style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 20px;"><span style="font-family: arial, helvetica, sans-serif;">Berenang</span></li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 60px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan :</span></p>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 60px;"><span style="font-family: arial, helvetica, sans-serif;">Kegiatan-kegiatan fiisk yang dilakukan dapat membantu anda dalam memanage tenaga anak yang lebih. Kegiatan fisik dapat disesuaikan dengan kondisi.</span></p>
<ol start="3" type="A">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;"><strong>Diet Makanan&nbsp;</strong></span></p>
</li>
</ol>
<ol>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Menghindari gula dan allergen (gandum, susu dan telur)</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Mengkonsumsi vitamin dan mikronutrien serta makanan yang mengandung asam lemak (omega-3)</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 76px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Dapat diganti dengan bahan-bahan yang rendah gula</span></p>