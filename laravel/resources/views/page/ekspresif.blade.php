<p><span style="font-family: arial, helvetica, sans-serif;">Kemampuan bahasa ekspresif&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">1. Tahap I : Menunjuk sesuatu yang diinginkan&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
    <li><span style="font-family: arial, helvetica, sans-serif;">Menunjuk objek kesulitan anak</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Menunjuk objek kesukaan dan bukan kesukaan anak</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Meletakkan kedua objek di atas meja</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Intruksi dapat dilakukan dengan menyebut nama anak &ldquo;(nama anak), mau apa?&rdquo; dan diharapkan respon anak adalah anak menunjuk objek yang disukai&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">2. Tahap II : Menunjuk sesuatu yang diinginkan secara spontan&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
    <li><span style="font-family: arial, helvetica, sans-serif;">Anak menunjuk kearah ibjek yang diinginkan tanpa instruksi verbal&nbsp;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : coba dengan menggunakan tiga objek terlebih dahulu&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">3. Tahap III : Imitasi suara dan kata&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Papa&rdquo;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Mama&rdquo;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Mami&rdquo;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Babi&rdquo;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Kuda&rdquo;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Sapi&rdquo;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Mobil&rdquo;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Motor&rdquo;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Aku&rdquo;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;Kamu&rdquo;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Respon yang diharapkan pada anak adalah anak dapat menirukan dengan benar&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">4. Tahap IV : Melabel Objek&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
    <li><span style="font-family: arial, helvetica, sans-serif;">Melabel 10 macam objek&nbsp;</span></li>
</ol>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">5. Tahap V : Menyebutkan nama gambar&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
    <li><span style="font-family: arial, helvetica, sans-serif;">Menyebutkan nama-nama 10 macam gambar&nbsp;</span></li>
</ol>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">6. Tahap VI : Mengatakan objek yang diinginkan&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
    <li><span style="font-family: arial, helvetica, sans-serif;">Menunjuk dan menyebutkan objek yang diinginkan, minimal tiga (siapakan tiga macam benda kesukaan anak)</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Instruksi yang dapat digunakan &ldquo;kamu ingin apa?&rdquo; dan diharapkan respon anak adalah menjawab &ldquo;(bu), saya ingin &hellip;&rdquo;&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">7. Tahap VII : Menyatakan ingin atau tidak atas suatu objek dengan kata-kata &ldquo;ya&rdquo; dan &ldquo;tidak&rdquo;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
    <li><span style="font-family: arial, helvetica, sans-serif;">Menyatakan &ldquo;ya&rdquo; atas objek yang disukai</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Menyatakan &ldquo;tidak&rdquo; atas objek yang tidak disukai</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Memilih objek yang disukai (objek yang disukai dan yang tidak disukai dicampur)</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Instruksi dapat menggunakan &ldquo;kamu mau&hellip;?&rdquo; dan diahrapkan respon dari anak adalah &ldquo;ya&rdquo; atau &ldquo;tidak&rdquo;. Pertanyaan dapat dikembangkan sendiri oleh orangtua disesuaikan dengan kondisi anak.&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">8. Tahap VIII : Melabel anggota keluarga atau orang dekat&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
    <li><span style="font-family: arial, helvetica, sans-serif;">Melabel anggota keluarga atau orang dekat secara langsung atau menggunakan fotonya (minimal 3 orang)</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : instruksi dapat menggunakan &ldquo;siapa ini?&rdquo; dan diharapkan respon anak adalah dapat menyebutkan nama yang ditanyakan&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">9. Tahap IX : Membuat pilihan&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
    <li><span style="font-family: arial, helvetica, sans-serif;">Memilih objek yang (sangat) disukai dan yang (sangat) tidak disukai (minimal tiga objek)</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : instruksi dapat menggunakan &ldquo; mau ini atau ini?&rdquo; disesuaikan dengan bahasa yang digunakan sehari-hari dan diharapkan respon anak adalah dapat menunjuk serta membuat label.&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">10. Tahap X : Saling menyapa&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
    <li><span style="font-family: arial, helvetica, sans-serif;">&nbsp;Mulai dengan &ldquo;halo &hellip; (nama)&rdquo;&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Memberi salam &ldquo;selamat pagi &hellip; (nama)&rdquo;&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Memberi salam &ldquo;selamat siang &hellip; (nama)&rdquo;&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Memberi salam &ldquo;selamat sore &hellip; (nama)&rdquo;&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Memberi salam &ldquo;selamat malam &hellip; (nama)&rdquo;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : instruksi dapat digunakan dengan &ldquo;tirukan&hellip;!&rdquo; dan diharapkan respon anak adalah menirukan dengan benar&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">11. Tahap XI : Menjawab pertanyaan sosial&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;siapa namamu?&rdquo;&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;berapa umurmu?&rdquo;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;apa kabar &hellip; (nama anak)?&rdquo;&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;dimanakah rumahmu?&rdquo;&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;siapakah kakak/adikmu?&rdquo;&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;kamu mau main dengan siapa?&rdquo;&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;siapakah nama papa/mamamu?&rdquo;&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;kamu mau makan apa ?&rdquo;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;di manakah sekolahmu ?&rdquo;&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;siapakh temanmu?&rdquo;&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;berapakah nomor teleponmu?&rdquo;&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;kamu mau minum apa?&rdquo;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;kapankah hari ulang tahunmu?&rdquo;&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;siapakah nama gurumu?&rdquo;&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;apakah acara tv yang kamu sukai?&rdquo;&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">&ldquo;kamu senang bermain apa?&rdquo;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Anak dapat dibantu untuk menjawab dengan benar . Anak dapat dibantu untuk mengingatnya. Lanjutkan hungga anak mampu menjawab secara mandiri.&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">12. Tahap XII : Menyebutkan kata kerja alam, gambar, orang lain dan diri sendiri&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
    <li><span style="font-family: arial, helvetica, sans-serif;">Berdiri&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Duduk&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Bertepuk tangan&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Bergoyang&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Makan&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Minum&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Bertepuk atau berbalik&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Melompat&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Memeluk&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Mencium&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Meniup&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Tidur&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Mengetuk&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Membaca&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Menggambar&nbsp;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Instruksi yang digunakan dapat berupa pertanyaan seperti &ldquo;apa yang ia kerjakan?&rdquo; , &ldquo;saya sedang apa?&rsquo; atau &ldquo;kamu sedang apa?&rdquo; dan diharapkan respon anak sesuai dengan label yang diberikan kepada pekerjaan atau kegiatan yang ditunjuk.&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">13. Tahap XIII : Melabel benda-benda melalui fungsinya&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
    <li><span style="font-family: arial, helvetica, sans-serif;">Menulis dengan &hellip; (pensil)&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Minum dari &hellip; (cangkir/gelas)</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Makan dengan &hellip; (sendok)&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Menggunting dengan &hellip; (gunting)&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Memabaca &hellip; (buku)&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Tidur di atas &hellip; (tempat tidur)&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Duduk diatas &hellip; (kursi)&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Berbicara melalui &hellip; (telepon)</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Mewarnai dengan &hellip; (krayon)&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Mencuci dengan &hellip; (sabun)&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Menyapu dengan &hellip; (sapu)&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Menyeka mulut dengan &hellip; (kertas tisu)&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Melempar &hellip; (bola)&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Menyisir rambut dengan &hellip; (sisir)&nbsp;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Objek dapat diganti dengan yang sudah dikenal anak, instruksi yang digunakan &ldquo;kamu menulis dengan apa?&rdquo; atau &nbsp;&ldquo;apa yang kamu lempar?&rdquo; atau &ldquo;saya duduk di atas apa?&rdquo; dan diharapkan anak dapat melabel atau menyebutkan kegiatannya dengan benar.&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">14. Tahap XIV : Melabel kepemilikan&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
    <li><span style="font-family: arial, helvetica, sans-serif;">Siapkan seseorang dengan pakaian yang cukup lengkap dan segera lakukan generalisasi dengan orang lain&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Menyentuh atau memegang bagian-bagian tubuh atau pakaian seseorang&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Menggunakan sufiks; -ku, -mu, -nya</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Instruksi dapat menggunakan &ldquo;baju siapa ini?&rdquo; dan dapat dikembangkan sesuai dengan kondisi. Respon yang diharapkan dari anak dapat menyebutkan objek dan pemiliknya&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">15. Tahap XV : Melabel rasa manis, asin, asam, pahit dan tawar</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
    <li><span style="font-family: arial, helvetica, sans-serif;">Merasakan rasa manis&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Merasakan rasa asin&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Merasakan rasa asam&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Merasakan rasa pahit&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Merasakan rasa tawar&nbsp;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : setiap poin rasa dapat disesuaikan dengan kondisi atau dapat dicontohkan air gula untuk rasa manis, air garam untuk rasa asin, air cuka untuk rasa asam, air kopi untuk rasa pahit dan air putih untuk rasa tawar. Instruksi yang digunakan dapat dengan &ldquo;apa rasanya?&rdquo; dengan hasil respon anak yaitu anak mampu mengungkapkan rasa dari objek yang dikecap</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">16. Tahap XVI : Melabel sensasi halus dan kasar&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
    <li><span style="font-family: arial, helvetica, sans-serif;">Merasakan butiran halus tepung&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Merasakan butiran kasar pasir&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Merasakan sentuhan halus kapas&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Merasakan sentuhan kasar sikat&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Merasakan permukaan halus kaca&nbsp;</span></li>
    <li><span style="font-family: arial, helvetica, sans-serif;">Merasakan permukaan kasar kertas ampelas&nbsp;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Instruksi yang digunakan bisa dengan &ldquo;bagaimana rasanya?&rdquo; dan respon anak diharapkan mampu menjawab dengan benar. </span></p>