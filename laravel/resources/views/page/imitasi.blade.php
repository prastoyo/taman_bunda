<ol style="list-style-type: upper-alpha;" start="1">
<li><span style="font-family: arial, helvetica, sans-serif;"><strong>Kemampuan menirukan (Imitasi) &nbsp;</strong></span></li>
</ol>
<ol style="list-style-type: decimal; margin-left: 0.25in;">
<li><span style="font-family: arial, helvetica, sans-serif;">Tahap I : Imitasi gerakan motorik kasar&nbsp;</span></li>
</ol>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
<li><span style="font-family: arial, helvetica, sans-serif;">Menepuk meja&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menepuk tangan&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Melambaikan tangan&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengangkat tangan&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengentakkan kaki&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menepuk paha&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menggeleng&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengangguk&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Berputar/menengok&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menutup muka dengan dua tangan&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menepuk bahu&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Melompat&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Bersidekap&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menepuk perut&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Berjalan di tempat&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Merentangkan tangan&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengetuk meja&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Bertolak pinggang&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menggosok-gosok kedua tangan&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menepuk kepala&nbsp;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : instruksi semua latihan imitasi harus sama yaitu &ldquo;tirukan&rdquo; serentak dengan gerakan model (contoh) dan diharapakn respon anak mampu menirukan model secara mandiri. Setiap aktivitas diusahakan berhasil terlebih dahulu untuk dapat dilanjutkan ke aktivitas selanjutnya.&nbsp;</span></p>
<ol style="list-style-type: undefined; margin-left: 0.25in;">
<li><span style="font-family: arial, helvetica, sans-serif;">Tahap II : imitasi aksi terhadap objek&nbsp;</span></li>
</ol>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
<li><span style="font-family: arial, helvetica, sans-serif;">Meletakkan blok-blok ke dalam keranjang&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Membunyikan bel&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mendorong mobil mainan&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Melambaikan bendera&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memukul drum&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memasang topi&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menggaruk&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menggosok mulut&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memukul palu (mainan)&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memainkan kecrek&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">(pura-pura) memberi makanan boneka&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengangkat telepon&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Minum dari cangkir&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Meniup terompet&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menyisir rambut&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menidurkan boneka&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menggulingkan benda (bola)&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Memasukkan koin ke dalam celengan&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mencium boneka&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Membubuhkan stempel&nbsp;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : instruksi semua latihan imitasi harus sama yaitu &ldquo;tirukan&rdquo; serentak dengan gerakan model (contoh) dan diharapakn respon anak mampu menirukan model secara mandiri. Setiap aktivitas diusahakan berhasil terlebih dahulu untuk dapat dilanjutkan ke aktivitas selanjutnya.&nbsp;</span></p>
<ol style="list-style-type: undefined; margin-left: 0.25in;">
<li><span style="font-family: arial, helvetica, sans-serif;">Tahap III : Imitasi gerakan motorik halus&nbsp;</span></li>
</ol>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
<li><span style="font-family: arial, helvetica, sans-serif;">Menggosokkan ibu jari ke jari-jari lain&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Membuka dan menutup tangan&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menepukkan kedua jari telunjuk&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menggoyangkan jari-jari tangan&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menggosokkan jari-jari ke ibu jari&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menepukkan jari-jari ke ibu jari (tangan lain)&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menunjuk bagian-bagian tubuh&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menunjukkan jari telunjuk ke telapak tangan&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Merentangkan jari telunjuk (menunjuk)&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengacungkan jempol&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Bersalaman&nbsp;</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt 1.0in;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : instruksi semua latihan imitasi harus sama yaitu &ldquo;tirukan&rdquo; serentak dengan gerakan model (contoh) dan diharapakn respon anak mampu menirukan model secara mandiri. Setiap aktivitas diusahakan berhasil terlebih dahulu untuk dapat dilanjutkan ke aktivitas selanjutnya.&nbsp;</span></p>
<p style="margin-left: 40px;"><span style="font-family: arial, helvetica, sans-serif;">2. Imitasi gerakan motorik mulut&nbsp;</span></p>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="1">
<li><span style="font-family: arial, helvetica, sans-serif;">Membuka mulut&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menjulurkan lidah&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengatupkan bibir&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Menggertakan gigi&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">MeniupInstruksi : dapat dilakukan terlebih dahulu dengan menggunakan bahan yang mudah ditiup seperti kertas, kapas atau nyala api lillin&nbsp;</span></li>
</ol>
<ol style="list-style-type: lower-alpha; margin-left: 0.5in;" start="6">
<li><span style="font-family: arial, helvetica, sans-serif;">Tersenyum&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Meringis&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mencium meletakkan lidah ke gigi atas&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mengigit bibir bawah&nbsp;</span><br /><span style="font-family: arial, helvetica, sans-serif;">Catatan : instruksi semua latihan imitasi harus sama yaitu &ldquo;tirukan&rdquo; serentak dengan gerakan model (contoh) dan diharapakn respon anak mampu menirukan model secara mandiri. Setiap aktivitas diusahakan berhasil terlebih dahulu untuk dapat dilanjutkan ke aktivitas selanjutnya.&nbsp;</span></li>
</ol>