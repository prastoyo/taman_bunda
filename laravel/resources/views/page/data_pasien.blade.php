@include('layouts.header')
@include('layouts.sidebar')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data Pasien</h1>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->

                <!--/.col (left) -->
                <!-- right column -->
                @foreach ($users as $user)
         
                @if(!$user->biodata)
                <div class="card">

                    <!-- /.card-header -->


                    <div class="card-body ">
                            <ul class="products-list product-list-in-card" style="width:300px">

                                <li class="item">
                                    <div class="product-img">
                                        <img src="https://www.weact.org/wp-content/uploads/2016/10/Blank-profile.png"
                                            alt="Product Image" class="img-size-50">
                                    </div>
                                    <div class=" product-info">
                                        <a href="javascript:void(0)" class="product-title">{{ $user->biodata->nama }}
                                            <span class="badge badge-success float-right">ada</span></a>
                                        <span class="product-description">
                                         
                                        </span>
                                    </div>
                                </li>
                                <!-- /.item -->

                                <!-- /.item -->
                            </ul>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer text-center">

                        <a href="#modalDetail" class="uppercase modalDetail" data-toggle="modal"
                                data-target="#modalDetail" 
                                data-nama="{{ $user->biodata->nama }}"
                                data-nama_ibu="{{ $user->biodata->nama_ibu }}"
                                data-tanggal_lahir_ibu="{{ $user->biodata->tanggal_lahir_ibu }}"
                                data-usia_ibu="{{ $user->biodata->usia_ibu }}"
                                data-pekerjaan_ibu="{{ $user->biodata->pekerjaan_ibu }}"
                                data-nama_anak="{{ $user->biodata->nama_anak }}"
                                data-tanggal_lahir_anak="{{ $user->biodata->tanggal_lahir_anak }}"
                                data-usia_anak="{{ $user->biodata->usia_anak }}"
                                data-riwayat_kelahiran="{{ $user->biodata->riwayat_kelahiran }}"
                                data-bantuan_melahirkan="{{ $user->biodata->bantuan_melahirkan }}"
                                data-riwayat_penyakit_ibu="{{ $user->biodata->riwayat_penyakit_ibu }}"
                                data-ibu_merokok="{{ $user->biodata->ibu_merokok }}"
                                data-ibu_akohol="{{ $user->biodata->ibu_akohol }}"
                                data-ibu_obat="{{ $user->biodata->ibu_obat }}"
                                data-kelainan_kehamilan="{{ $user->biodata->kelainan_kehamilan }}"
                                data-usia_anak_membeo="{{ $user->biodata->usia_anak_membeo }}"
                                data-usia_anak_berjalan="{{ $user->biodata->usia_anak_berjalan }}"
                                data-riwayat_sakit_anak="{{ $user->biodata->riwayat_sakit_anak }}"
                                data-id="{{ $user->biodata->user_id }}">Lihat Data Detail</a>
                    </div>
                    <div class="card-footer text-center">

                        <a href="{{ url('delete_pasien/' . $user->id) }}" class="uppercase">Hapus Data</a>
                    </div>



                   



                    <!-- /.card-footer -->
                </div>
           

                @endif
                @endforeach




                <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Detail Data</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="#" method="POST">
                               @csrf 
                                <input type="hidden" value="" name="id" id="id">
                                <input type="hidden" value="" name="id" id="id">
                                <div class="form-group">
                                    <label for="name">1. Nama</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="nama" id="nama">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">2. Nama ibu</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="nama_ibu" id="nama_ibu">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">3. Tanggal lahir ibu </label>
                                    <div class="col-md-12">
                                        <input class="form-control" type="date" name="tanggal_lahir_ibu"
                                            id="tanggal_lahir_ibu">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">4. Usia ibu </label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="usia_ibu" id="usia_ibu">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">5. Pekerjaan ibu </label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="pekerjaan_ibu"
                                            id="pekerjaan_ibu">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">6. Nama anak </label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="nama_anak" id="nama_anak">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">7. Tanggal lahir anak </label>
                                    <div class="col-md-12">
                                        <input id="tanggal_lahir_anak" type="date" class="form-control"
                                            name="tanggal_lahir_anak">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">8. Usia anak </label>
                                    <div class="col-md-12">
                                        <input id="usia_anak" type="text" class="form-control" name="usia_anak">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">9. Riwayat kelahiran (normal/Caesar) </label>
                                    <div class="col-md-12">
                                        <input id="riwayat_kelahiran" type="text" class="form-control"
                                            name="riwayat_kelahiran">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">10. Ibu melahirkan dengan bantuan (dokter
                                        kandungan/bidan/lainnya …) </label>
                                    <div class="col-md-12">
                                        <input id="bantuan_melahirkan" type="text" class="form-control"
                                            name="bantuan_melahirkan">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">11. Riwayat penyakit ibu </label>
                                    <div class="col-md-12">
                                        <input id="riwayat_penyakit_ibu" type="text" class="form-control"
                                            name="riwayat_penyakit_ibu">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">12. Apakah ibu merokok dalam pada saat kehamilan
                                        (ya/tidak) </label>
                                    <div class="col-md-12">
                                        <input id="ibu_merokok" type="text" class="form-control"
                                            name="ibu_merokok">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">13. Apakah ibu mengkonsumsi alcohol pada saat kehamilan
                                        (ya/tidak) </label>
                                    <div class="col-md-12">
                                        <input id="ibu_akohol" type="text" class="form-control"
                                            name="ibu_akohol">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">14. Apakah ibu mengkonsumsi obat-obatan pada saat
                                        kehamilan (ya/tidak) </label>
                                    <div class="col-md-12">
                                        <input id="ibu_obat" type="text" class="form-control" name="ibu_obat">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">15. Pada saat kehamilan apakah terdapat kelainan
                                        (ya/tidak)</label>
                                    <div class="col-md-12">
                                        <input id="kelainan_kehamilan" type="text" class="form-control"
                                            name="kelainan_kehamilan">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">16 Usia berapa anak mulai mengeluarkan suara atau membeo?
                                    </label>
                                    <div class="col-md-12">
                                        <input id="usia_anak_membeo" type="text" class="form-control"
                                            name="usia_anak_membeo">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">17 Usia berapa anak mulai berjalan? </label>
                                    <div class="col-md-12">
                                        <input id="usia_anak_berjalan" type="text" class="form-control"
                                            name="usia_anak_berjalan">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">18. Riwayat sakit anak?</label>
                                    <div class="col-md-12">
                                        <input id="riwayat_sakit_anak" type="text" class="form-control"
                                            name="riwayat_sakit_anak">
                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            {{-- <button type="submit" class="btn btn-primary">Update</button> --}}
                        </div>
                    </form>

                    </div>
                </div>
            </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@include('layouts.footer')



<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{ url('template/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ url('template/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ url('template/plugins/select2/js/select2.full.min.js') }}"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="{{ url('template/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
<!-- InputMask -->
<script src="{{ url('template/plugins/moment/moment.min.js') }}"></script>
<script src="{{ url('template/plugins/inputmask/jquery.inputmask.min.js') }}"></script>
<!-- date-range-picker -->
<script src="{{ url('template/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap color picker -->
<script src="{{ url('template/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ url('template/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- Bootstrap Switch -->
<script src="{{ url('template/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
<!-- BS-Stepper -->
<script src="{{ url('template/plugins/bs-stepper/js/bs-stepper.min.js') }}"></script>
<!-- dropzonejs -->
<script src="{{ url('template/plugins/dropzone/min/dropzone.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ url('template/dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ url('template/dist/js/demo.js') }}"></script>
<!-- Page specific script -->
<script>
    $('#modalDetail').on('shown.bs.modal', function() {
        $('#modalDetail').trigger('focus')
    })


    $(document).on("click", ".modalDetail", function() {
        var nama = $(this).data('nama');
        var nama_ibu = $(this).data('nama_ibu');
        var tanggal_lahir_ibu = $(this).data('tanggal_lahir_ibu');
        var usia_ibu = $(this).data('usia_ibu');
        var pekerjaan_ibu = $(this).data('pekerjaan_ibu');
        var nama_anak = $(this).data('nama_anak');
        var tanggal_lahir_anak = $(this).data('tanggal_lahir_anak');
        var usia_anak = $(this).data('usia_anak');
        var riwayat_kelahiran = $(this).data('riwayat_kelahiran');
        var bantuan_melahirkan = $(this).data('bantuan_melahirkan');
        var riwayat_penyakit_ibu = $(this).data('riwayat_penyakit_ibu');
        var ibu_merokok = $(this).data('ibu_merokok');
        var ibu_akohol = $(this).data('ibu_akohol');
        var ibu_obat = $(this).data('ibu_obat');
        var kelainan_kehamilan = $(this).data('kelainan_kehamilan');
        var usia_anak_membeo = $(this).data('usia_anak_membeo');
        var usia_anak_berjalan = $(this).data('usia_anak_berjalan');
        var riwayat_sakit_anak = $(this).data('riwayat_sakit_anak');
        var id = $(this).data('id');

        $(".modal-body #nama").val(nama);
        $(".modal-body #nama_ibu").val(nama_ibu);
        $(".modal-body #tanggal_lahir_ibu").val(tanggal_lahir_ibu);
        $(".modal-body #usia_ibu").val(usia_ibu);
        $(".modal-body #pekerjaan_ibu").val(pekerjaan_ibu);
        $(".modal-body #nama_anak").val(nama_anak);
        $(".modal-body #tanggal_lahir_anak").val(tanggal_lahir_anak);
        $(".modal-body #usia_anak").val(usia_anak);
        $(".modal-body #riwayat_kelahiran").val(riwayat_kelahiran);
        $(".modal-body #bantuan_melahirkan").val(bantuan_melahirkan);
        $(".modal-body #riwayat_penyakit_ibu").val(riwayat_penyakit_ibu);
        $(".modal-body #ibu_merokok").val(ibu_merokok);
        $(".modal-body #ibu_akohol").val(ibu_akohol);
        $(".modal-body #ibu_obat").val(ibu_obat);
        $(".modal-body #kelainan_kehamilan").val(kelainan_kehamilan);
        $(".modal-body #usia_anak_membeo").val(usia_anak_membeo);
        $(".modal-body #usia_anak_berjalan").val(usia_anak_berjalan);
        $(".modal-body #riwayat_sakit_anak").val(riwayat_sakit_anak);
        $(".modal-body #id").val(id);


        // As pointed out in comments, 
        // it is unnecessary to have to manually call the modal.
        // $('#addBookDialog').modal('show');
    });

    $(function() {
        //Initialize Select2 Elements
        $('.select2').select2()

        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {
            'placeholder': 'dd/mm/yyyy'
        })
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {
            'placeholder': 'mm/dd/yyyy'
        })
        //Money Euro
        $('[data-mask]').inputmask()

        //Date range picker
        $('#reservationdate').datetimepicker({
            format: 'L'
        });
        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'MM/DD/YYYY hh:mm A'
            }
        })
        //Date range as a button
        $('#daterange-btn').daterangepicker({
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                        'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate: moment()
            },
            function(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format(
                    'MMMM D, YYYY'))
            }
        )

        //Timepicker
        $('#timepicker').datetimepicker({
            format: 'LT'
        })

        //Bootstrap Duallistbox
        $('.duallistbox').bootstrapDualListbox()

        //Colorpicker
        $('.my-colorpicker1').colorpicker()
        //color picker with addon
        $('.my-colorpicker2').colorpicker()

        $('.my-colorpicker2').on('colorpickerChange', function(event) {
            $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
        });

        $("input[data-bootstrap-switch]").each(function() {
            $(this).bootstrapSwitch('state', $(this).prop('checked'));
        });

    })
    // BS-Stepper Init
    document.addEventListener('DOMContentLoaded', function() {
        window.stepper = new Stepper(document.querySelector('.bs-stepper'))
    });

    // DropzoneJS Demo Code Start
    Dropzone.autoDiscover = false;

    // Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
    var previewNode = document.querySelector("#template");
    previewNode.id = "";
    var previewTemplate = previewNode.parentNode.innerHTML;
    previewNode.parentNode.removeChild(previewNode);

    var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
        url: "/target-url", // Set the url
        thumbnailWidth: 80,
        thumbnailHeight: 80,
        parallelUploads: 20,
        previewTemplate: previewTemplate,
        autoQueue: false, // Make sure the files aren't queued until manually added
        previewsContainer: "#previews", // Define the container to display the previews
        clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
    });

    myDropzone.on("addedfile", function(file) {
        // Hookup the start button
        file.previewElement.querySelector(".start").onclick = function() {
            myDropzone.enqueueFile(file);
        };
    });

    // Update the total progress bar
    myDropzone.on("totaluploadprogress", function(progress) {
        document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
    });

    myDropzone.on("sending", function(file) {
        // Show the total progress bar when upload starts
        document.querySelector("#total-progress").style.opacity = "1";
        // And disable the start button
        file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
    });

    // Hide the total progress bar when nothing's uploading anymore
    myDropzone.on("queuecomplete", function(progress) {
        document.querySelector("#total-progress").style.opacity = "0";
    });

    // Setup the buttons for all transfers
    // The "add files" button doesn't need to be setup because the config
    // `clickable` has already been specified.
    document.querySelector("#actions .start").onclick = function() {
        myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
    };
    document.querySelector("#actions .cancel").onclick = function() {
        myDropzone.removeAllFiles(true);
    };

    // DropzoneJS Demo Code End

</script>

<script>
    $.widget.bridge('uibutton', $.ui.button)

</script>
</body>

</html>
