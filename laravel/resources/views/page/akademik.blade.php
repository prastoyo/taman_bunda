<ol style="list-style-type: upper-alpha;" start="5">
<li><span style="font-family: arial, helvetica, sans-serif;"><strong>Kemampuan membantu diri (<em>self help skills</em>)&nbsp;</strong></span></li>
</ol>
<ol style="list-style-type: decimal; margin-left: 0.25in;">
<li><span style="font-family: arial, helvetica, sans-serif;">Tahap I : Minum dari gelas</span></li>
</ol>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt .75in;"><span style="font-family: arial, helvetica, sans-serif;">Dalam kegiatan self help skills ini aktivitas anak dicontohkan satu kegiatan yang dapat dikembangkand alam kegiatan yang lainnya.&nbsp;</span></p>
<p style="line-height: 115%; font-size: 15px; font-family: 'Calibri',sans-serif; margin: 0in 0in .0001pt .75in;"><span style="font-family: arial, helvetica, sans-serif;">Petunjuk 1:&nbsp;</span></p>
<ol style="list-style-type: decimal; margin-left: 0.75in;">
<li><span style="font-family: arial, helvetica, sans-serif;">Lihat bagian &ldquo;mengajarkan kemampuan membantu diri&rdquo; hal. 31&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Siapkan semua peralatan yang diperlukan&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mulai dengan prompt penuh dan kemudian kurangi prompt secara bertahap&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Mulai dengan tahap akhir dan lanjutikan bertahap ke tahao awal aktivitas&nbsp;</span></li>
</ol>
<p style="margin: 0in 0in 10pt 68px; line-height: 115%; font-size: 15px; font-family: Calibri, sans-serif;"><span style="font-family: arial, helvetica, sans-serif;">Petunjuk 2:&nbsp;</span></p>
<ol style="list-style-type: decimal; margin-left: 0.75in;">
<li><span style="font-family: arial, helvetica, sans-serif;">Amati dan catatlah waktu di mana anak biasa buang air besar atau keil di celana&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Di waktu-waktu tersebu, antisipasilah agar anak tidak membuang air besar atau kecil di celana dengan cara membawanya ke toilet&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Sabarlah menunggu saat anak buang air besar atau kecil lalu segera berikan imbalan yang mengesankan kepada anak&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Lanjutkan dengan bertanya sev=cara verbal sampai anak mampu mengangguk atau menjawab</span></li>
</ol>
<p style="margin: 0in 0in 10pt 68px; line-height: 115%; font-size: 15px; font-family: Calibri, sans-serif;"><span style="font-family: arial, helvetica, sans-serif;">Petunjuk 3:&nbsp;</span></p>
<ol style="list-style-type: decimal; margin-left: 0.75in;">
<li><span style="font-family: arial, helvetica, sans-serif;">Dalam memulai aktivitas menggosok gigi, berikan terlebih dahulu contoh dan perkenalkan rasa pasta gigi ke mulut anak&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Lakukan pompt penuh dan urangi secara bertahao sampai tanpa prompt&nbsp;</span></li>
<li><span style="font-family: arial, helvetica, sans-serif;">Segera berikan imblan meskipun anak hanya menggosok satu duka kali saja</span></li>
</ol>