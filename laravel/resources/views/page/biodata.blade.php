<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Teman Bunda</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <!-- date-range-picker -->
    <script src="{{url('template/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <link rel="stylesheet" href="{{url('template/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{url('template/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{url('template/dist/css/adminlte.min.css')}}"><!-- daterange picker -->
    <link rel="stylesheet" href="{{url('template/plugins/daterangepicker/daterangepicker.css')}}">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{url('template/plugins/daterangepicker/daterangepicker.css')}}">

</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="#">Biodata</a>
        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Silahkan Untuk Mengisikan Form Biodata</p>

                <form method="POST" action="{{ route('input_biodata') }}">
                    @csrf

                    <div class="form-group">
                        <label for="name">1. Nama</label>
                        <div class="col-md-12">
                            <input id="name" type="text" class="form-control" name="nama">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">2. Nama ibu</label>
                        <div class="col-md-12">
                            <input id="name" type="text" class="form-control" name="nama_ibu">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">3. Tanggal lahir ibu </label>
                        <div class="col-md-12">
                        <input class="form-control" type="date" name="tanggal_lahir_ibu" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">4. Usia ibu </label>
                        <div class="col-md-12">
                            <input id="name" type="text" class="form-control" name="usia_ibu">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">5. Pekerjaan ibu </label>
                        <div class="col-md-12">
                            <input id="name" type="text" class="form-control" name="pekerjaan_ibu">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">6. Nama anak </label>
                        <div class="col-md-12">
                            <input id="name" type="text" class="form-control" name="nama_anak">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">7. Tanggal lahir anak </label>
                        <div class="col-md-12">
                            <input id="name" type="date" class="form-control" name="tanggal_lahir_anak">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">8. Usia anak </label>
                        <div class="col-md-12">
                            <input id="name" type="text" class="form-control" name="usia_anak">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">9. Riwayat kelahiran (normal/Caesar) </label>
                        <div class="col-md-12">
                            <input id="name" type="text" class="form-control" name="riwayat_kelahiran">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">10. Ibu melahirkan dengan bantuan (dokter kandungan/bidan/lainnya …) </label>
                        <div class="col-md-12">
                            <input id="name" type="text" class="form-control" name="bantuan_melahirkan">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">11. Riwayat penyakit ibu </label>
                        <div class="col-md-12">
                            <input id="name" type="text" class="form-control" name="riwayat_penyakit_ibu">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">12. Apakah ibu merokok dalam pada saat kehamilan (ya/tidak) </label>
                        <div class="col-md-12">
                            <input id="name" type="text" class="form-control" name="ibu_merokok">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">13. Apakah ibu mengkonsumsi alcohol pada saat kehamilan (ya/tidak) </label>
                        <div class="col-md-12">
                            <input id="name" type="text" class="form-control" name="ibu_akohol">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">14. Apakah ibu mengkonsumsi obat-obatan pada saat kehamilan (ya/tidak) </label>
                        <div class="col-md-12">
                            <input id="name" type="text" class="form-control" name="ibu_obat">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">15. Pada saat kehamilan apakah terdapat kelainan (ya/tidak)</label>
                        <div class="col-md-12">
                            <input id="name" type="text" class="form-control" name="kelainan_kehamilan">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">16 Usia berapa anak mulai mengeluarkan suara atau membeo? </label>
                        <div class="col-md-12">
                            <input id="name" type="text" class="form-control" name="usia_anak_membeo">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">17 Usia berapa anak mulai berjalan? </label>
                        <div class="col-md-12">
                            <input id="name" type="text" class="form-control" name="usia_anak_berjalan">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">18. Riwayat sakit anak?</label>
                        <div class="col-md-12">
                            <input id="name" type="text" class="form-control" name="riwayat_sakit_anak">
                        </div>
                    </div>


                    <div class="form-group mb-0">
                        <div class="col-md-12 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Submit') }}
                            </button>
                        </div>
                    </div>
                </form>


                <!-- /.social-auth-links -->


            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->

    <!-- jQuery -->
    <script src="{{url('template/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{url('template/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{url('template/dist/js/adminlte.min.js')}}"></script>
    <!-- date-range-picker -->
    <script src="{{url('template/plugins/daterangepicker/daterangepicker.js')}}"></script>

    <script>
        //Date range picker
        $('#reservationdate').datetimepicker({
            format: 'L'
        });
        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'MM/DD/YYYY hh:mm A'
            }
        })
        //Date range as a button
        $('#daterange-btn').daterangepicker({
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate: moment()
            },
            function(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
            }
        )
    </script>

</body>

</html>