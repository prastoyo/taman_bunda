<ol type="A">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;"><strong>Tingkat dasar pembentukan kontak mata&nbsp;</strong></span></p>
</li>
</ol>
<ol>
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap I : Duduk mandiri di kuris dan berdiri mandiri</span></p>
</li>
</ol>
<ol type="a">
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Duduk mandiri di kuris atas intruksi</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 76px;"><span style="font-family: arial, helvetica, sans-serif;">Petunjuk : Kepatuhan dibentuk dengan intruksi &ldquo;duduk&rdquo; atau &ldquo;berdiri&rdquo;. Dilakukan tanpa meja dan prompt (hadiah) segera diberikan setelah instruksi</span></p>
<ol start="2" type="a">
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Berdiri mandiri atas intruksi</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 76px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : Mengajarkan kepatuhan duduk dan berdiri atas instruksi jangan dilakukan bersamaan, tetapi dilakukan sendiri-sendiri agar anak tidak bingung</span></p>
<ol start="2">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap II : kontak mata dengan instruksi &ldquo;lihat&rdquo;</span></p>
</li>
</ol>
<ol type="a">
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Membuat kontak mata selama satu detik</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 76px;"><span style="font-family: arial, helvetica, sans-serif;">Petunjuk : pertama kali dapat memakai umpan makanan atau minuman yang diletakkan 5 cm di depan mata orangtua. Umpan dapat langsung dijadikan sebagai imbalan</span></p>
<ol start="2" type="a">
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Membuat kontak amat selama lima detik</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Membuat kontak mata saat dipanggil namanya ketika bermain</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Membuat kontak mata dari jauh dengan memanggil namanya</span></p>
</li>
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Merespon instruksi &ldquo;(nama anak) lihat!&rdquo;</span></p>
</li>
</ol>
<ol start="3">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap III : kontak mata ketika diperintah</span></p>
</li>
</ol>
<ol type="a">
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Membuat kontak mata dengan instruksi &ldquo;Lihat ini&rdquo;</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 76px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : dalam tahapan ini anak benar-benar sudah harus lulus pada tahap II</span></p>
<ol start="4">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap IV : Merespon instruksi</span></p>
</li>
</ol>
<ol type="a">
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Merespon instruksi &ldquo;tangan dilipat&rdquo; atau &ldquo;duduk rapi</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 76px;"><span style="font-family: arial, helvetica, sans-serif;">Catatan : untuk pembentukan kepatuhan dan kontak mata ini, prompt harus segera dilakukan setelah instruksi tanpa memakai urutan siklus</span></p>
<ol start="5">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap V : Kontak mata saat nama anak dipanggil (intermediate)</span></p>
</li>
</ol>
<ol type="a">
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Membuat kontak mata selama lima detik</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 76px;"><span style="font-family: arial, helvetica, sans-serif;">Instruksi panggil nama anak dan diharapkan respon anak adalah anak mampu membuat kontak mata selama lima detik</span></p>
<ol start="6">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap VI : kontak mata saat nama anak dipanggil ketika bermain (intermediate)</span></p>
</li>
</ol>
<ol type="a">
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Membuat kontak mata selama lima detik</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 76px;"><span style="font-family: arial, helvetica, sans-serif;">Instruksi panggil nama anak dan diharapkan respon anak adalah anak mampu membuat kontak mata selama lima detik</span></p>
<ol start="7">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap VII : kontak mata saat nama anak dipanggil dari jarak jauh</span></p>
</li>
</ol>
<ol type="a">
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Membuat kontak ma_ta selama lima detik</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 76px;"><span style="font-family: arial, helvetica, sans-serif;">Instruksi panggil nama anak dan diharapkan respon anak adalah anak mampu membuat kontak mata selama lima detik</span></p>
<ol start="8">
<li>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Tahap VIII : menanyakan &ldquo;apa?&rdquo; saat nama dipanggil</span></p>
</li>
</ol>
<ol type="a">
<li style="margin-left: 20px;">
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">Membuat kontak mata selama lima detik</span></p>
</li>
</ol>
<p style="margin-bottom: 0.14in; line-height: 115%; text-align: left; margin-left: 76px;"><span style="font-family: arial, helvetica, sans-serif;">Instruksi panggil nama anak dan diharapkan respon anak adalah anak mampu membuat kontak mata selama lima detik</span></p>