  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
 

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
        <a href="#" class="d-block">Teman Bunda</a>
        </div>
      </div> 

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
         
         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

               <li class="nav-item ">
            <a href="{{url('/home')}}" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>Beranda</p>
            </a>
          </li>
          @if(auth()->user()->role == "client")

          <li class="nav-item ">
            <a href="{{ route('belajar_autis') }}" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>Belajar Autis </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('deteksi_autis') }}" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>Deteksi Autis </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('belajar_adhd') }}" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>Belajar ADHD</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('deteksi_adhd') }}" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>Deteksi Adhd</p>
            </a>
          </li>
          {{-- <li class="nav-item">
            <a href="#modalDetail"  data-toggle="modal"
            data-target="#modalDetail" 
            data-nama="{{ $user->biodata->nama }}"
            data-nama_ibu="{{ $user->biodata->nama_ibu }}"
            data-tanggal_lahir_ibu="{{ $user->biodata->tanggal_lahir_ibu }}"
            data-usia_ibu="{{ $user->biodata->usia_ibu }}"
            data-pekerjaan_ibu="{{ $user->biodata->pekerjaan_ibu }}"
            data-nama_anak="{{ $user->biodata->nama_anak }}"
            data-tanggal_lahir_anak="{{ $user->biodata->tanggal_lahir_anak }}"
            data-usia_anak="{{ $user->biodata->usia_anak }}"
            data-riwayat_kelahiran="{{ $user->biodata->riwayat_kelahiran }}"
            data-bantuan_melahirkan="{{ $user->biodata->bantuan_melahirkan }}"
            data-riwayat_penyakit_ibu="{{ $user->biodata->riwayat_penyakit_ibu }}"
            data-ibu_merokok="{{ $user->biodata->ibu_merokok }}"
            data-ibu_akohol="{{ $user->biodata->ibu_akohol }}"
            data-ibu_obat="{{ $user->biodata->ibu_obat }}"
            data-kelainan_kehamilan="{{ $user->biodata->kelainan_kehamilan }}"
            data-usia_anak_membeo="{{ $user->biodata->usia_anak_membeo }}"
            data-usia_anak_berjalan="{{ $user->biodata->usia_anak_berjalan }}"
            data-riwayat_sakit_anak="{{ $user->biodata->riwayat_sakit_anak }}"
            data-id="{{ $user->biodata->user_id }}" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>Edit Biodata</p>
            </a>
          </li> --}}

          @elseif(auth()->user()->role == "admin")

          <li class="nav-item">
            <a href="{{ route('data_pasien') }}" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>Data Pasien</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('data_dokter') }}" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>Data Dokter</p>
            </a>
          </li>

          @else
          <li class="nav-item">
            <a href="{{ route('list_pasien') }}" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>Kontak Pasien</p>
            </a>
          </li>
          @endif
        <li class="nav-item">
            <a onclick="event.preventDefault(); document.getElementById('logout-form').submit();" href="{{ route('logout') }}" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>Logout </p>
            </a>
          </li>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
         
        </ul>
      </nav>
    </div>
    <!-- /.sidebar -->
  </aside>