@extends('layouts.app')

@section('content')
<div class="container">
    <chat-component :user="{{ auth()->user() }}" :id="{{ json_encode($id) }}"></chat-component>
</div>
@endsection 